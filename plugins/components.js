import Vue from 'vue'
import Page from "~/components/Page.vue"
import Logo from "~/components/Logo.vue"
import Banner from "~/components/Banner.vue"
import Register from "~/components/Register.vue"
import Story from "~/components/Story.vue"
import Footer from "~/components/Footer.vue"
import School from "~/components/School.vue"

Vue.component('Page', Page)
Vue.component('Logo', Logo)
Vue.component('Banner', Banner)
Vue.component('Register', Register)
Vue.component('Story', Story)
Vue.component('Footer', Footer)
Vue.component('School', School)